package it.siw.ProgettoSiw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

//import it.siw.ProgettoSiw.model.Tizio;
//import it.siw.ProgettoSiw.service.TizioService;

@Controller
@ComponentScan("it.siw.progetto.CentriFormazione.service")
public class ProvaController {
	
//	@Autowired //si "attacca da solo"
//	private TizioService tService;
//	
//	//saluta un tizio presente nel db, se non lo trova saluta gennaro
//	@RequestMapping("/salutaTizio/{idTizio}")
//	public String saluta(@PathVariable String idTizio, Model m){
//		Long id=Long.parseLong(idTizio);
//		System.out.println("\n\narrivatoo\n\n");
//		Tizio t;
//		if(this.tService.trovaDaId(id)==null) {
//			t=new Tizio("gennaro", "capracotta");
//			m.addAttribute("tizioDaSalutare", t);
//		}else
//			m.addAttribute("tizioDaSalutare", this.tService.trovaDaId(id));
//		return "paginaSaluta";
//	}
	

}
