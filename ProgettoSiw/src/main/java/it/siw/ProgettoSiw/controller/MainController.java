package it.siw.ProgettoSiw.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import it.siw.ProgettoSiw.model.Centro;
import it.siw.ProgettoSiw.service.CentroService;

@Controller
public class MainController {
	
	@Autowired
	private CentroService centroService;
	
	@RequestMapping("/")
	public String redirectToLogin() {
		return "redirect:/login";
	}
	
	@RequestMapping("/login")
	public String login(Authentication auth) {
		boolean isAdminCentro=false;
		boolean isAdminGlobale = false;
		if(auth!=null) {
			Collection<? extends GrantedAuthority> authorities=auth.getAuthorities();
			for (GrantedAuthority a : authorities) {
				if(a.getAuthority().equals("ROLE_ADMIN")) {
					isAdminGlobale=true;
				}
				if(a.getAuthority().equals("ROLE_ADMIN_CENTRO")) {
					isAdminCentro=true;
				}
				
			}
		}
		if(isAdminCentro) {
			System.out.println("\n\nloggato come amministratore centro\n\n");
			String num;
			String nomeUtenteCorrente=auth.getName();
			
			Centro c=centroService.FindByUsernameUtente(nomeUtenteCorrente);
			if (c==null) return "login";
			num= c.getId().toString();		
			
			return "redirect:/amministrazione-centro/"+num;
		}
		else if(isAdminGlobale) {
			System.out.println("\n\nloggato come amministratore globale\n\n");
			return "redirect:/amministrazione-globale";
		}
		return "login";
	}
	
	@RequestMapping("/login-error")
	public String loginErr(Model m) {
		m.addAttribute("error", true);
		return "login";
	}
//	@RequestMapping("/logout")
//	public String logout() {
//		return "/login";
//	}
}
