package it.siw.ProgettoSiw.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import it.siw.ProgettoSiw.model.Centro;
import it.siw.ProgettoSiw.service.CentroService;

@Controller
public class CentroController {
	@Autowired
	private CentroService centroService;

	@RequestMapping("/amministrazione-centro/{idCentro}")
	private String paginaCentro(@PathVariable String idCentro, Model m) {
		Long id = Long.parseLong(idCentro);
		Centro c= centroService.findById(id);
		m.addAttribute("centroCorrente", c);
		return "amministratoreCentro";
	}
	
	
}
