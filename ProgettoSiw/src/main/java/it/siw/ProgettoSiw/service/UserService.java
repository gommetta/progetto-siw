package it.siw.ProgettoSiw.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.siw.ProgettoSiw.repository.UserRepository;
import it.siw.ProgettoSiw.security.Utente;

@Service
@ComponentScan(basePackages="it.siw.ProgettoSiw.repository")
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Transactional
	public void save(Utente u) {
		this.userRepository.save(u);
	}
}
