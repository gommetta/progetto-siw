package it.siw.ProgettoSiw.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.siw.ProgettoSiw.repository.PrivilegeRepository;
import it.siw.ProgettoSiw.security.Privilege;

@Service
public class PrivilegeService {

	
	@Autowired
	private PrivilegeRepository privilegeRepository;

	public Privilege findByName(String name) {
		for (Privilege privilege : this.privilegeRepository.findAll()) {
			if(privilege.getName().equals(name))
				return privilege;
		}
		return null;
	}

	@Transactional
	public void save(Privilege privilege) {
		this.privilegeRepository.save(privilege);
		
	}
	
	
}
