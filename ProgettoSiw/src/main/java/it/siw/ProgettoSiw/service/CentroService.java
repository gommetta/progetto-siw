package it.siw.ProgettoSiw.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.siw.ProgettoSiw.model.Allievo;
import it.siw.ProgettoSiw.model.Attivita;
import it.siw.ProgettoSiw.model.Centro;
import it.siw.ProgettoSiw.repository.CentroRepository;
import it.siw.ProgettoSiw.security.Utente;

@Service
public class CentroService {

	
	@Autowired
	private CentroRepository centroRepository;
	
	public Centro findById(Long id) {
		return centroRepository.findOne(id);
	}
	
	public List<Centro> findALL(){
		//return centroRepository.findAll();
		List<Centro> list = new ArrayList<>();
		(centroRepository.findAll().iterator()).forEachRemaining(list::add);
		return list;
	}
	
	public Centro FindByUsernameUtente(String u) {
		List<Centro> list = this.findALL();
		
		for (Centro centro : list) {
			if(centro.getResponsabile().getUsername().compareTo(u)==0) return centro;
			
		}
		return null;
	}
	
	@Transactional
	public void save(Centro c) {
		this.centroRepository.save(c);
	}
	
}
