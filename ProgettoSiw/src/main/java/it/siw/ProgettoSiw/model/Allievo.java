package it.siw.ProgettoSiw.model;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Allievo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Column
	private String nome;
	@Column(nullable=true)
	private String email;
	@Column(nullable=true)
	private String tel;
	@Column(nullable=true)
	private String luogoNascita;
	@Column(nullable=true)
	@Temporal(TemporalType.DATE)
	private Calendar dataNascita;
	

	@ManyToMany
	private List<Attivita> attivitaIscritto;

	public Allievo() {}
	public Allievo(String nome,String email,String tel,String luogoNascita, Calendar dataNascita){
		this.nome= nome;
		this.email= email;
		this.tel= tel;
		this.luogoNascita=luogoNascita;
		this.dataNascita= dataNascita;
		
	}
	public boolean equals(Object o) {
		Allievo a =(Allievo) o ;
		return a.getNome().equals(this.getNome()) && a.getEmail().equals(this.getEmail());
	}
	
	public int hashCode() {
		return this.getNome().hashCode() + this.getEmail().hashCode();
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getLuogoNascita() {
		return luogoNascita;
	}
	public void setLuogoNascita(String luogoNascita) {
		this.luogoNascita = luogoNascita;
	}

	public Calendar getDataNascita() {
		return dataNascita;
	}

	public void setDataNascita(Calendar dataNascita) {
		this.dataNascita = dataNascita;
	}

	public List<Attivita> getAttivitaIscritto() {
		return attivitaIscritto;
	}

	public void setAttivitaIscritto(List<Attivita> attivitaIscritto) {
		this.attivitaIscritto = attivitaIscritto;
	}

	public Long getId() {
		return id;
	}
	
}
