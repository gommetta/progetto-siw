package it.siw.ProgettoSiw.repository;

import org.springframework.data.repository.CrudRepository;

import it.siw.ProgettoSiw.model.Allievo;

public interface AllievoRepository extends CrudRepository<Allievo, Long> {

}
