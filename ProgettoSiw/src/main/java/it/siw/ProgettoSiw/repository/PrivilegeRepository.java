package it.siw.ProgettoSiw.repository;

import org.springframework.data.repository.CrudRepository;

import it.siw.ProgettoSiw.security.Privilege;

public interface PrivilegeRepository extends CrudRepository<Privilege, Long> {

}
