package it.siw.ProgettoSiw.repository;

import org.springframework.data.repository.CrudRepository;

import it.siw.ProgettoSiw.model.Attivita;

public interface AttivitaRepository extends CrudRepository<Attivita, Long> {

}
