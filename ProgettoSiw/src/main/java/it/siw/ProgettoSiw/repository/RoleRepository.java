package it.siw.ProgettoSiw.repository;

import org.springframework.data.repository.CrudRepository;

import it.siw.ProgettoSiw.security.Role;

public interface RoleRepository extends CrudRepository<Role, Long>{

}
