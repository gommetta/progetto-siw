package it.siw.ProgettoSiw.security;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
//@Table(name="roles")
public class Role {
  
	public Role() {}
    public Role(String name) {
		this.name = name;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
 
	@Column(name="role")
    private String name;
    //uno a molti forse
    @ManyToMany(mappedBy = "roles")
    private Collection<Utente> users;
 
    @ManyToMany
//    @JoinTable(
//        name = "roles_privileges", 
//        joinColumns = @JoinColumn(
//          name = "role_id", referencedColumnName = "id"), 
//        inverseJoinColumns = @JoinColumn(
//          name = "privilege_id", referencedColumnName = "id"))
    private Collection<Privilege> privileges; 
    
    
    public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Collection<Utente> getUsers() {
		return users;
	}

	public Collection<Privilege> getPrivileges() {
		return privileges;
	}

	public void setPrivileges(Collection<Privilege> privileges) {
		this.privileges = privileges;
		
	}
}
