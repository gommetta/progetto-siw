package it.siw.ProgettoSiw.service;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import it.siw.ProgettoSiw.model.Allievo;
import it.siw.ProgettoSiw.service.AllievoService;

import org.junit.Before;

public class AllievoServiceTest {

	@Autowired
	private AllievoService allievoService;
	
	@Before
	public void setUp() throws Exception {
		Allievo a1=new Allievo("Gigi",null,null,null,null);
		Allievo a2=new Allievo("Aldo",null,null,null,null);
		Allievo a3=new Allievo("Mimmi",null,null,null,null);
		Allievo a4=new Allievo("Lara",null,null,null,null);
		this.allievoService.save(a1);
		this.allievoService.save(a2);
		this.allievoService.save(a3);
		this.allievoService.save(a4);		
	}
	
//	@Test
//	public void test() {
//		fail("Not yet implemented");
//	}
	
	@Test
	public void testFindAll() {
		assertEquals(this.allievoService.findAll().size(), 4);
		
	}

}
